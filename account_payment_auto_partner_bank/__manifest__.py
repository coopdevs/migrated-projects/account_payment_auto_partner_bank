{
    'name': "Odoo Account Payment Auto Partner Bank",
    'version': '12.0.1.0.1',
    'depends': ['account_payment_partner'],
    'author': "Coopdevs Treball SCCL",
    'website': 'https://coopdevs.org',
    'category': "Banking addons",
    'description': """
    Changes parter_bank_id to payment_mode default
    when it's fixed and manual transfer mode.
    """,
    "license": "AGPL-3",
    'data': [
    ],
}
